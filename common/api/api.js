import $http from "../http.js"

export const getLabel = (data) => {
	return $http({
		url: 'getLabel',
		data
	})
}

export const getArticle = (data) => {
	return $http({
		url: 'getArticle',
		data
	})
}

export const updateLike = (data) => {
	return $http({
		url: 'updateLike',
		data
	})
}

export const searchArticle = (data) => {
	return $http({
		url: 'searchArticle',
		data
	})
}

export const updateUserLabel = (data) => {
	return $http({
		url: 'updateUserLabel',
		data
	})
}

export const getArticelDetail = (data) => {
	return $http({
		url: 'getArticelDetail',
		data
	})
}

export const updateComment = (data) => {
	return $http({
		url: 'updateComment',
		data
	})
}

export const getComment = (data) => {
	return $http({
		url: 'getComment',
		data
	})
}

export const updateAuthor = (data) => {
	return $http({
		url: 'updateAuthor',
		data
	})
}

export const updateThumbsUp = (data) => {
	return $http({
		url: 'updateThumbsUp',
		data
	})
}

export const getFollow = (data) => {
	return $http({
		url: 'getFollow',
		data
	})
}

export const getAuthors = (data) => {
	return $http({
		url: 'getAuthors',
		data
	})
}

export const getUser = (data) => {
	return $http({
		url: 'getUser',
		data
	})
}

export const getMyArticle = (data) => {
	return $http({
		url: 'getMyArticle',
		data
	})
}

export const addFeedback = (data) => {
	return $http({
		url: 'addFeedback',
		data
	})
}










