// 批量导出文件, 将其他js文件中的对象导出
const requireApi = require.context(
	// api的相对路径
	'.',
	// 是否查询子目录
	false,
	// 查询文件,匹配js
	/.js$/
)

let module = {}
requireApi.keys().forEach((key, index) => {
	if (key === './index.js') {
		return
	}
	Object.assign(module, requireApi(key))
})

export default module
