## uni-app 快速入门 从零开始实现新闻资讯类跨端应用-有需要资料的小伙伴可以加群242733211 
**1.去掉顶部原生导航栏 "navigationStyle":"custom"** 

 **2.设置顶部状态栏文字颜色 "navigationBarTextStyle":"white"** 

 **3.[easyCom]组件命名规则:components/组件名/组件名.vue,满足规则可以省略import和注册,直接使用** 

 **4.动态行内样式 :style="{height: statusBarHeight+'px'}"** 

 **5.封装request方法,批量导出云函数** 

 **6.自定义导航栏组件navbar,固定定位在顶部,动态设置状态栏高度和导航栏高度,并设置一个空view占位状态栏+导航栏的高度,使不遮挡下面内容** 

 **7.使用字体图标插件uni-icons,需要安装** 

 **8.卡片阴影 box-shadow: 0 0 5px 1px rgba($color: #000000, $alpha: 0.1);** 

 **9.文字溢出隐藏** 

 **10.自定义竖直滚动组件 scroll-list** 

 **11.自定义三种模式(基础,多图,大图)卡片组件 card-list** 

 **12.使用轮播图实现手势滑动组件 swiper-list** 

 **13.组件生命周期中只能使用create(),不能用onLoad()** 

 **14.this.$set() 对象赋值,强制Vue刷新页面数据** 

 **15.使用loadMore插件,需要安装** 

 **16.scroll-view中@scrolltolower="loadmore"下拉加载更多** 

 **17.强制渲染页面 this.$forceUpdate()** 

 **18.swpier ==> scroll-view 分页,下拉加载更多** 

 **19.云数据库操作,追加字段,in:数组是否包含某个字段** 

 **20.vuex的使用** 

 **21.父组件中的子组件使用v-model,在子组件的props中也可以接收,实现组件和组件之间的双向绑定**
 
 **22.tabbar跳转uni.switchTab** 

 **23.数据持久化到本地缓存 uni.getStorageSync("_history")** 

 **24.自定义事件 发送uni.$emit('updateLabel') 接收uni.$on('updateLabel',(res=>{})** 

 **25.富文本插件 gaoyia-parse import uParse from '@/components/gaoyia-parse/parse.vue'** 

 **26.弹出层组件 uni-popup, 依赖 uni-transition 过渡动画** 

 **27.回复组件递归调用 <comment-box :item='i'></comment-box>** 

 **28.云函数值返回数组 .unwind('$comments')** 

 **29.对象复制 let temp = JSON.parse(JSON.stringify(this.comments))** 

 **30.comment-box中计算属性 filters, 时间格式化 formatTime(time)** 

 **31.图片背景虚化 my-header-background** 

 **32.三等分布局 feedback-img-box** 

 **33.上传图片 feedback** 