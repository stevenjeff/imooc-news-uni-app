'use strict';
const db = uniCloud.database();

exports.main = async (event, context) => {
	const {user_id} = event
	
	let res = await db.collection('user').doc(user_id).get()
	
	//返回数据给客户端
	return {
		code:200,
		msg:'请求成功',
		data:res.data[0]
	}
};
