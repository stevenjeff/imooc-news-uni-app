'use strict';
const db = uniCloud.database();
const $ = db.command.aggregate
exports.main = async (event, context) => {
	// 通过分类查询
	let {
		user_id,
		name,
		page = 1,
		pageSize = 10
	} = event
	let params = {}
	if (name !== '全部') {
		params = {
			classify: name
		}
	}
	const user = await db.collection('user').doc(user_id).get()
	const article_likes_ids = user.data[0].article_likes_ids
	
	// 聚合: 更精细化的去处理数据, 求和/分组,指定返回哪些字段 
	let res = await db.collection('article')
		.aggregate()
		// 追加字段
		.addFields({
			// in:数组是否包含某个字段 (当前表的_id字段)
			is_like:$.in(['$_id',article_likes_ids])
		})
		.match(params)
		.project({
			content: false
		})
		.skip((page - 1) * pageSize)
		.limit(pageSize)
		.end()

	return {
		code: 200,
		msg: '请求成功',
		data: res.data
	}
};
