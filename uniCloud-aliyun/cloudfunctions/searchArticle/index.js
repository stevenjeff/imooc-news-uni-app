'use strict';
const db = uniCloud.database();

exports.main = async (event, context) => {
	let {
		value
	} = event

	// let res = await db.collection('article')
	// 	.field({
	// 		content:false
	// 	})
	// 	.get()

	// 聚合: 更精细化的去处理数据, 求和/分组,指定返回哪些字段
	let res = await db.collection('article')
		.aggregate()
		.project({
			content: false
		})
		.match({
			title:new RegExp(value)
		})
		.end()

	return {
		code: 200,
		msg: '请求成功',
		data: res.data
	}
};
