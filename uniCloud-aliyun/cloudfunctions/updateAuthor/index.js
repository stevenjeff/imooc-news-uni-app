'use strict';
const db = uniCloud.database();
const cmd = db.command

exports.main = async (event, context) => {
	const {user_id,author_id} = event
	
	let users = await db.collection('user').doc(user_id).get()
	let author_likes_ids = users.data[0].author_likes_ids
	
	let param = null
	// 数组包含某个元素
	if(author_likes_ids.includes(author_id)){
		// 移除元素,表示取消关注
		param = cmd.pull(author_id)
		// 更新被关注者的粉丝数量-1
		await db.collection('user').doc(author_id).update({
			fans_count:cmd.inc(-1)
		})
	} else{
		// 添加元素,表示关注
		param = cmd.addToSet(author_id)
		// 更新被关注者的粉丝数量+1
		await db.collection('user').doc(author_id).update({
			fans_count:cmd.inc(1)
		})
	}
	
	await db.collection('user').doc(user_id).update({
		author_likes_ids:param
	})
	
	//返回数据给客户端
	return {
		code:200,
		msg:'请求成功'
	}
};
