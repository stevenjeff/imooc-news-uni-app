'use strict';
const db = uniCloud.database();
// 数据库操作符
const cmd = db.command
exports.main = async (event, context) => {
	let {
		user_id,
		articleId
	} = event
	const user = await db.collection('user').doc(user_id).get()
	const article_likes_ids = user.data[0].article_likes_ids

	let opt = null
	if (article_likes_ids.includes(articleId)) {
		// 从数组中删除
		opt = cmd.pull(articleId)
	} else {
		// 向article_likes_ids数组添加元素
		opt = cmd.addToSet(articleId)
	}

	await db.collection('user').doc(user_id).update({
		article_likes_ids: opt
	})

	return {
		code: 200,
		msg: '请求成功',
		data: {}
	}
};
