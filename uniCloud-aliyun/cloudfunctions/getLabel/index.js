'use strict';
// 获取数据库引用
const db = uniCloud.database();
const $ = db.command.aggregate
exports.main = async (event, context) => {
	const {user_id, type} = event
	let user = await db.collection('user').doc(user_id).get()
	user = user.data[0]
	// let res =await db.collection('label').get()
	
	let param = {}
	// all查全部, 不是all就只查询我关注的标签
	if(type!=='all'){
		param={
			current:true
		}
	}
	let res =await db.collection('label')
	.aggregate()
	.addFields({
		// $.in判断当前字段是否在数组中   $表示当前数据
		current:$.in(['$_id',$.ifNull([user.label_ids,[]])])
	})
	.match(param)
	.end()
	
	//返回数据给客户端
	return {
		code:200,
		mag:'请求成功',
		data:res.data
	}
};
