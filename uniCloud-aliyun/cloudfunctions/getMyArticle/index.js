'use strict';
const db = uniCloud.database();
const $ = db.command.aggregate
const cmd = db.command

exports.main = async (event, context) => {
	const {user_id} = event
	
	let user = await db.collection('user').doc(user_id).get()
	user = user.data[0]
	
	let res = await db.collection('article')
	.aggregate()
	.addFields({
		is_like:$.in(['$_id',user.article_likes_ids])
	})
	.match({
		id:cmd.in(user.article_ids)
	})
	.end()
	
	return {
		code:200,
		msg:'请求成功',
		data:res.data
	}
};
