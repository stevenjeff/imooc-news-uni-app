'use strict';
const db = uniCloud.database();
const cmd =db.command

exports.main = async (event, context) => {
	const {user_id, article_id} = event
	
	let user = await db.collection('user').doc(user_id).get()
	let thumbs_up_article_ids = user.data[0].thumbs_up_article_ids
	
	let params = null
	if(thumbs_up_article_ids.includes(article_id)){
		return {
			code:200,
			msg:'您已经点过赞了'
		}
	} else{
		params = cmd.addToSet(article_id)
	}
	
	await db.collection('user').doc(user_id).update({
		thumbs_up_article_ids:params
	})
	
	await db.collection('article').doc(article_id).update({
		thumbs_up_count: cmd.inc(1)
	})
	
	return {
		code:200,
		msg:'点赞成功'
	}
};
