'use strict';
const db = uniCloud.database();
const $ = db.command.aggregate
exports.main = async (event, context) => {
	const {
		user_id,
		articleId,
		page = 1,
		pageSize = 10
	} = event

	let res = await db.collection('article')
		.aggregate()
		.match({
			_id: articleId
		})
		.unwind('$comments')
		.project({
			_id: 0, //默认会返回ID
			comments: 1
		})
		.replaceRoot({
			newRoot: '$comments'
		})
		.skip((page-1)*pageSize)
		.limit(pageSize)
		.end()
	//返回数据给客户端
	return {
		code: 200,
		mag: '请求成功',
		data: res.data
	}
};
