'use strict';
const db = uniCloud.database();

exports.main = async (event, context) => {
	const {user_id, labelList} = event
	await db.collection('user').doc(user_id).update({
		label_ids:labelList
	})
	
	//返回数据给客户端
	return {
		code:200,
		msg:'请求成功'
	}
};
